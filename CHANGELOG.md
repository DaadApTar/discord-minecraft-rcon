## Version 1.0
Created bot.  
Added log module.  
Added command `/rcon`. This command allows send commands to server.  
Added comamnd `/reconnect`. This command allows reconnect RCON.  
Added command `/stop`. This command stops server.  
Removed random symbols in response.  
Made discord's emotes fewer in minecraft chat.

***

## Version 1.1
Transfered config to yml.  
Added commands `/reload`. This command reloads config.yml  
Moved src/main.py to app.py
