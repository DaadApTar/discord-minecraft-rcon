import src.RCON as RCON
import src.discordbot as discordbot
from src.parser import TOKEN

def main():
    RCON.init()
    RCON.Timer()
    discordbot.init(TOKEN)
    RCON.stop()

if __name__ == "__main__":
    main()
