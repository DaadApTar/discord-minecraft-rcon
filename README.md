# What is it.
A Discord bot that establishes a RCON connection to the Minecraft server.
It is needed to use the game console directly from Discord.
# How to use it.
You should create your own discord application in [Discord Developer portal](https://discord.com/developers/applications). 
Then you should open your application and in page Bot take a token. Put the token 
in `config.yml` in token.
```yml
token: "YOUR TOKEN HERE"
```
Make your Discord account a developer. Then RBM to channel and click the button "Copy Channel ID". 
Put Channel's ID in `config.yml` in channelID.
```yml
channelID: CHANNEL'S ID HERE
```
Copy RCON.IP in your hosting and put it in RCONIp.
```yml
RCONIp: "RCON IP HERE"
```
Copy RCON.password in your hosting and put in in RCONPassword.
```yml
RCONPassword: "YOUR PASSWORD HERE"
```
Copy RCON.port in your hosting and put it in RCONPort.
```yml
RCONPort: YOUR PORT HERE
```
RBM to user and click the button "Copy User ID". Then put User's ID in permittedUsers. You can 
list users by comma.
```yml
permittedUsers: [ID, ID, ID]
```
RBM to user and click the button "Copy User ID". Then put User's ID in blockedUsers. You can 
list users by comma.
```yml
blockedUsers: [ID, ID, ID]
```
RBM to user and click the button "Copy User ID". Then put User's ID in allowedCommands and 
write commands that user can use. You can list permissions by comma.
```yml
allowedCommands: [
    ID: ["COMMAND", "COMMAND", "COMMAND"],
    ID: ["COMMAND", "COMMAND"]
]
```
RBM to user and click the button "Copy User ID". Then put User's ID in blockedCommands and 
write commands that user can't use. You can list permissions by comma.
```yml
blockedCommands: [
    ID: ["COMMAND", "COMMAND", "COMMAND"],
    ID: ["COMMAND", "COMMAND"]
]
```
Enter time in seconds to reloadDelay for set timeout for auto reloading RCON connection.
```yml
reloadDelay: SECONDS
```
