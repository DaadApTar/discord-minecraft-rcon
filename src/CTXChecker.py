import discord
import src.parser as parser

def check(ctx: discord.Interaction, command = ""):
    id = ctx.user.id

    for obj in parser.ALLOWEDCOMMANDS:
        if id in obj:
            commands = obj[id]
            for cmd in commands:
                if command.startswith(cmd):
                    return True
    
    for obj in parser.BLOCKEDCOMMANDS:
        if id in obj:
            commands = obj[id]
            for cmd in commands:
                if command.startswith(cmd):
                    return False

    return (ctx.channel_id == parser.CHANNELID or ctx.user.id in parser.PERMITTEDUSERS) and ctx.user.id not in parser.BLOCKEDUSERS
