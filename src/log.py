from datetime import datetime
import os

path=r"./logs/"
if not os.path.exists(path):
    os.makedirs(path)

f = open(path + datetime.today().strftime("%Y-%m-%d_%H:%M:%S") + ".log", "w+")
latest = open(f"{path}latest.log", "w+")

def log(value):
    now = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    print(f"[{now} INFO]: {value}")
    f.write(f"[{now} INFO]: {value}\n")
    latest.write(f"[{now} INFO]: {value}\n")

def error(value):
    now = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    print(f"[{now} ERROR]: {value}")
    f.write(f"[{now} ERROR]: {value}\n")
    latest.write(f"[{now} ERROR]: {value}\n")

