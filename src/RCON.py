from mctools import RCONClient
from threading import Thread
import time
import src.log as log
import src.parser as parser

def init():
    global rcon
    log.log("Starting RCON connection.")
    try:
        rcon = RCONClient(parser.RCONIP, parser.RCONPORT)
        rcon.login(parser.RCONPASSWORD)
    except:
        log.error("Failed to connect.")
    if rcon.is_authenticated():
        log.log("RCON has successfuly authentificated.")
        return True
    log.error("RCON hasn't authentificated.")
    return False

def command(command):
    try:
        response = rcon.command(command, length_check=False)
        return response
    except:
        auth()
        return False

def auth():
    success = init()
    return success

def stop():
    log.log("Closing RCON connection.")
    rcon.stop()

class Timer(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
    def run(self):
        while True:
            time.sleep(parser.TIMER)
            try:
                global rcon
                rcon = RCONClient(parser.RCONIP, parser.RCONPORT)
                rcon.login(parser.RCONPASSWORD)
            except:
                log.error("RCON hasn't authentificated.")
