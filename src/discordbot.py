import discord
import re
import src.RCON as RCON
import src.log as log
from src.CTXChecker import check
from discord.ext import commands
import src.parser as parser

bot = commands.Bot(command_prefix="$", intents=discord.Intents.all())

def init(token: str):
    log.log("Starting bot.")
    try:
        bot.run(token)
    except:
        log.error("Can not run bot. Check your bot token.")

@bot.event
async def on_ready():
    log.log(f"Bot is up and ready as {bot.user}.")
    try:
        synced = await bot.tree.sync()
        log.log(f"Synced {len(synced)} command(s).")
    except Exception as e:
        print(e)
    
@bot.tree.command(name="rcon")
@discord.app_commands.describe(command= "RCON")
async def rcon(ctx: discord.Interaction, command: str):
    channel = ctx.channel
    user = ctx.user
    embed = discord.Embed(color=0x991144)
    if check(ctx, command):
        log.log(f"{user.name} executed command: `{command}` in {channel}.")
        command = re.sub(r"(?<=\:)([0-9]{19})(?=\>)", "",command)
        RCONresponse = re.sub(r"\[[0-9;]+m", "", str(RCON.command(command)).replace("[0m", ""))
        try:
            if (RCONresponse == "False"):
                embed.add_field(name="Error", value="Reconnecting.")
                RCON.auth()
            else:
                embed.color = 0x199969
                embed.add_field(name="Success", value="Here's a response:")
                embed.add_field(name="response", value=f"{RCONresponse}")
        except:
            pass
        await ctx.response.send_message(embed=embed, silent=True)
    else:
        embed.add_field(name="Error", value="You can't use it here!")
        log.log(f"{user.name} tried to execute a command: `{command}` in {channel}.")
        await ctx.response.send_message(embed=embed)

@bot.tree.command(name="reconnect")
async def reconnect(ctx: discord.Interaction):
    channel = ctx.channel
    embed = discord.Embed()
    embed.color = 0x991144
    if check(ctx):
        log.log(f"{ctx.user.name} is reconnecting RCON.")
        success = RCON.auth()
        if success:
            embed.color = 0x199969
            embed.add_field(name="Success", value="Authetificated")
        else:
            embed.add_field(name="Error", value="Can't reconnect to the RCON server.")
        await ctx.response.send_message(embed=embed, silent=True)
    else:
        log.log(f"{ctx.user.name} tried to reconnect RCON in {channel}.")
        embed.add_field(name="Error", value="You can't use it here!")
        await ctx.response.send_message(embed=embed)

@bot.tree.command(name="stop")
async def stop(ctx: discord.Interaction):
    channel = ctx.channel
    embed = discord.Embed()
    embed.color = 0x199969
    embed.add_field(name="Success", value="Stopping...")
    if check(ctx):
        log.log(f"{ctx.user.name} is stopping the bot.")
        await ctx.response.send_message(embed=embed, silent=True)
        await bot.close()
    else:
        log.log(f"{ctx.user.name} tried to stop bot in {channel}.")
        embed.color = 0x991144
        embed.add_field(name="Error", value="You can't use it here!")
        await ctx.response.send_message(embed=embed)

@bot.tree.command(name="reload")
async def reload(ctx: discord.Interaction):
    channel = ctx.channel
    embed = discord.Embed()
    embed.color = 0x199969
    if check(ctx):
        log.log(f"{ctx.user.name} is reloading the config.")
        success = parser.reload()
        if success:
            embed.add_field(name="Success", value="Configs were loaded.")
        else:
            embed.color = 0x991144
            embed.add_field(name="Error", value="Something went wrong.")
        await ctx.response.send_message(embed=embed, silent=True)
    else:
        log.log(f"{ctx.user.name} tried to reload config in {channel}.")
        embed.color = 0x991144
        embed.add_field(name="Error", value="You can't use it here!")
        await ctx.response.send_message(embed=embed)
