import yaml
import src.log as log

try:
    with open("config/config.yml", "r") as file:
        config = yaml.safe_load(file)

    TOKEN = config["token"]
    CHANNELID = config["channelID"]
    RCONIP = config["RCONIp"]
    RCONPASSWORD = config["RCONPassword"]
    RCONPORT = config["RCONPort"]
    PERMITTEDUSERS = config["permittedUsers"]
    BLOCKEDUSERS = config["blockedUsers"]
    ALLOWEDCOMMANDS = config["allowedCommands"]
    BLOCKEDCOMMANDS = config["blockedCommands"]
    TIMER = config["reloadDelay"]
    log.log("Config.yml was loaded.")
except:
    log.error("Can't load config.yml.")

def reload():
    try:
        with open("config/config.yml", "r") as file:
            config = yaml.safe_load(file)

        global TOKEN, CHANNELID, RCONIP, RCONPASSWORD, RCONPORT, PERMITTEDUSERS, BLOCKEDUSERS, ALLOWEDCOMMANDS, BLOCKEDCOMMANDS, TIMER
    
        TOKEN = config["token"]
        CHANNELID = config["channelID"]
        RCONIP = config["RCONIp"]
        RCONPASSWORD = config["RCONPassword"]
        RCONPORT = config["RCONPort"]
        PERMITTEDUSERS = config["permittedUsers"]
        BLOCKEDUSERS = config["blockedUsers"]
        ALLOWEDCOMMANDS = config["allowedCommands"]
        BLOCKEDCOMMANDS = config["blockedCommands"]
        TIMER = config["reloadDelay"]
        log.log("Config.yml was loaded.")
        return True
    except:
        log.error("Can't load config.yml.")
        return False
